package resource

import (
	"fmt"
	"strings"

	"github.com/valyala/fasthttp"
	"gitlab.bcowtech.de/bcow-go/httparg"
)

type EchoResource struct {
}

// argument types
type (
	GetEchoArgs struct {
		Voice string `query:"*voice"`
		Count int    `query:"count"`
	}
)

func (r *EchoResource) Init() {
}

func (r *EchoResource) Get(ctx *fasthttp.RequestCtx) {
	// handle panic errors
	defer func() {
		recover()
	}()

	args := GetEchoArgs{}
	httparg.NewProcessor(&args, httparg.ProcessorOption{
		ErrorHandleFunc: func(err error) {
			fmt.Printf("%% error: %+v\n", err)
			ctx.Error(err.Error(), fasthttp.StatusBadRequest)
			panic(err)
		},
	}).ProcessQueryString(ctx.QueryArgs().String())

	// this line should not be output if some errors occurred. e.g the argument "voice" is nothing.
	fmt.Println("% no errors occurred")

	if args.Count <= 0 {
		args.Count = 1
	}

	ctx.Success("text/plain", []byte(strings.Repeat(args.Voice, args.Count)))
}

func (r *EchoResource) Put(ctx *fasthttp.RequestCtx) {
	ctx.Error("OK", fasthttp.StatusInternalServerError)
}
