module webapiserver

go 1.14

require (
	github.com/andybalholm/brotli v1.0.1 // indirect
	github.com/valyala/fasthttp v1.16.0
	gitlab.bcowtech.de/bcow-go/config v1.0.0
	gitlab.bcowtech.de/bcow-go/host-fasthttp v1.1.0
	gitlab.bcowtech.de/bcow-go/httparg v1.0.0
)
