package resource

import (
	"fmt"

	. "webapiserver/internal"

	"github.com/valyala/fasthttp"
)

type SettingResource struct {
	ServiceProvider *ServiceProvider
}

var peekFormat = `Cache:
    Host: %s
    Password: %s
    DB: %d
    PoolSize: %d
    Workspace: %s`

func (r *SettingResource) Init() {
}

func (r *SettingResource) Peek(ctx *fasthttp.RequestCtx) {
	fmt.Fprintf(ctx, peekFormat,
		r.ServiceProvider.CacheServer.Host,
		r.ServiceProvider.CacheServer.Password,
		r.ServiceProvider.CacheServer.DB,
		r.ServiceProvider.CacheServer.PoolSize,
		r.ServiceProvider.Workspace,
	)
}
